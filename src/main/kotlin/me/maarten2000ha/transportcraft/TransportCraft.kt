package me.maarten2000ha.transportcraft

import me.maarten2000ha.transportcraft.registry.BlocksRegistry
import me.maarten2000ha.transportcraft.registry.ItemRegistry
import me.maarten2000ha.transportcraft.datagen.setupDataGen
import me.maarten2000ha.transportcraft.registry.EntityRegistry
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent
import net.minecraftforge.fml.event.lifecycle.FMLDedicatedServerSetupEvent
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import thedarkcolour.kotlinforforge.forge.FORGE_BUS
import thedarkcolour.kotlinforforge.forge.MOD_BUS

@Mod(TransportCraft.ID)
object TransportCraft {
    const val ID = "transportcraft"

    private val LOGGER: Logger = LogManager.getLogger(ID)

    init {
        LOGGER.log(Level.INFO, "Transport craft has started")
        ItemRegistry.register(MOD_BUS)
        BlocksRegistry.register(MOD_BUS)
        EntityRegistry.register(MOD_BUS)

        MOD_BUS.addListener(::setupDataGen)
        MOD_BUS.addListener(::onClientSetup)
        FORGE_BUS.addListener(::onDedicatedServerSetup)
    }

    private fun onClientSetup(event: FMLClientSetupEvent) {
        LOGGER.log(Level.INFO, "Initializing client...")
    }

    private fun onDedicatedServerSetup(event: FMLDedicatedServerSetupEvent) {
        LOGGER.log(Level.INFO, "Server starting...")
    }
}