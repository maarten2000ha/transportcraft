package me.maarten2000ha.transportcraft.events

import me.maarten2000ha.transportcraft.TransportCraft
import me.maarten2000ha.transportcraft.registry.EntityRegistry
import me.maarten2000ha.transportcraft.renderer.ExampleEntityModel
import me.maarten2000ha.transportcraft.renderer.ExampleEntityRenderer
import net.minecraftforge.api.distmarker.Dist
import net.minecraftforge.client.event.EntityRenderersEvent
import net.minecraftforge.client.event.EntityRenderersEvent.RegisterLayerDefinitions
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus


@Mod.EventBusSubscriber(modid = TransportCraft.ID, bus = Bus.MOD, value = [Dist.CLIENT])
object ModEventClientBusEvents {

    @SubscribeEvent
    fun registerLayers(event: RegisterLayerDefinitions) {
        event.registerLayerDefinition(ExampleEntityModel.LAYER_LOCATION) {ExampleEntityModel.createBodyLayer()}
    }

    @SubscribeEvent
    fun registerRenderers(event: EntityRenderersEvent.RegisterRenderers) {
        event.registerEntityRenderer(EntityRegistry.EXAMPLE_ENTITY.get(), ::ExampleEntityRenderer)
    }
}

