package me.maarten2000ha.transportcraft.events

import me.maarten2000ha.transportcraft.TransportCraft
import me.maarten2000ha.transportcraft.entities.ExampleEntity
import me.maarten2000ha.transportcraft.registry.EntityRegistry
import net.minecraftforge.event.entity.EntityAttributeCreationEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus

@Mod.EventBusSubscriber(modid = TransportCraft.ID, bus = Bus.MOD)
object ModEventBus {

    @SubscribeEvent
    fun registerAttributes(event: EntityAttributeCreationEvent){
        event.put(EntityRegistry.EXAMPLE_ENTITY.get(), ExampleEntity.createAttributes().build())
    }

}