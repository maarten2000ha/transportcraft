package me.maarten2000ha.transportcraft.datagen

import me.maarten2000ha.transportcraft.TransportCraft
import net.minecraft.data.DataGenerator
import net.minecraft.data.tags.BlockTagsProvider
import net.minecraft.data.tags.ItemTagsProvider
import net.minecraftforge.common.data.ExistingFileHelper

class TransportCraftItemTags(gen: DataGenerator, blockTags: BlockTagsProvider,  helper: ExistingFileHelper): ItemTagsProvider(gen, blockTags, TransportCraft.ID, helper){
    override fun addTags() {
    }
}