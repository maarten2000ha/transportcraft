package me.maarten2000ha.transportcraft.datagen

import net.minecraftforge.forge.event.lifecycle.GatherDataEvent

fun setupDataGen(event: GatherDataEvent) {
    val generator = event.generator
    val existingFileHelper = event.existingFileHelper

    if (event.includeServer()) {
        generator.addProvider(TransportCraftRecipes(generator))
        generator.addProvider(TransportCraftLootTables(generator))
        val blockTags = TransportCraftBlockTags(generator, existingFileHelper)
        generator.addProvider(blockTags)
        generator.addProvider(TransportCraftItemTags(generator, blockTags, existingFileHelper))
    }

     if (event.includeClient()) {
         generator.addProvider(TransportCraftBlockState(generator, existingFileHelper))
         generator.addProvider(TransportCraftItemModels(generator, existingFileHelper))
         generator.addProvider(TransportCraftLanguageProvider(generator, "en_us"))
     }

//    DataGenerator generator = event.getGenerator();
//    if (event.includeServer()) {
//        generator.addProvider(new TutRecipes(generator));
//        generator.addProvider(new TutLootTables(generator));
//        TutBlockTags blockTags = new TutBlockTags(generator, event.getExistingFileHelper());
//        generator.addProvider(blockTags);
//        generator.addProvider(new TutItemTags(generator, blockTags, event.getExistingFileHelper()));
//        generator.addProvider(new TutBiomeTags(generator, event.getExistingFileHelper()));
//        generator.addProvider(new TutStructureSetTags(generator, event.getExistingFileHelper()));
//    }
//    if (event.includeClient()) {
//        generator.addProvider(new TutBlockStates(generator, event.getExistingFileHelper()));
//        generator.addProvider(new TutItemModels(generator, event.getExistingFileHelper()));
//        generator.addProvider(new TutLanguageProvider(generator, "en_us"));
//    }
}