package me.maarten2000ha.transportcraft.datagen

import me.maarten2000ha.transportcraft.TransportCraft
import net.minecraft.data.DataGenerator
import net.minecraft.data.tags.BlockTagsProvider
import net.minecraftforge.common.data.ExistingFileHelper

class TransportCraftBlockTags(gen: DataGenerator, helper: ExistingFileHelper) : BlockTagsProvider(gen, TransportCraft.ID, helper){

    override fun addTags() {
    }
}