package me.maarten2000ha.transportcraft.datagen

import me.maarten2000ha.transportcraft.TransportCraft
import net.minecraft.data.DataGenerator
import net.minecraftforge.client.model.generators.BlockStateProvider
import net.minecraftforge.common.data.ExistingFileHelper

class TransportCraftBlockState(gen: DataGenerator, helper: ExistingFileHelper) : BlockStateProvider(gen, TransportCraft.ID, helper) {

    override fun registerStatesAndModels() {
    }
}