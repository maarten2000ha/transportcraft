package me.maarten2000ha.transportcraft.datagen


import net.minecraft.data.DataGenerator
import net.minecraft.data.recipes.FinishedRecipe
import net.minecraft.data.recipes.RecipeProvider
import java.util.function.Consumer

class TransportCraftRecipes(gen: DataGenerator) : RecipeProvider(gen) {

    override fun buildCraftingRecipes(consumer: Consumer<FinishedRecipe>) {
    }
}