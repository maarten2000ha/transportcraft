package me.maarten2000ha.transportcraft.datagen

import me.maarten2000ha.transportcraft.TransportCraft
import me.maarten2000ha.transportcraft.registry.EntityRegistry
import me.maarten2000ha.transportcraft.registry.ItemRegistry
import net.minecraft.data.DataGenerator
import net.minecraftforge.common.data.LanguageProvider

class TransportCraftLanguageProvider(gen: DataGenerator, local: String): LanguageProvider(gen, TransportCraft.ID, local) {

    override fun addTranslations() {
        add("itemGroup." + TransportCraft.ID, "Transport craft")

        //Items
        add(ItemRegistry.CROWBAR.get(), "Crowbar")

        //Blocks

        //Entities
        add(EntityRegistry.EXAMPLE_ENTITY.get(), "Example Entity")
    }
}