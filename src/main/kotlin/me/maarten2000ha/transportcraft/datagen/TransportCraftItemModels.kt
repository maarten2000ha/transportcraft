package me.maarten2000ha.transportcraft.datagen

import me.maarten2000ha.transportcraft.TransportCraft
import me.maarten2000ha.transportcraft.registry.ItemRegistry
import net.minecraft.data.DataGenerator
import net.minecraftforge.client.model.generators.ItemModelProvider
import net.minecraftforge.common.data.ExistingFileHelper

class TransportCraftItemModels(gen: DataGenerator, helper: ExistingFileHelper) :
    ItemModelProvider(gen, TransportCraft.ID, helper) {

    override fun registerModels() {
//        withExistingParent(Registration.EXAMPLE_BLOCK_ITEM.get().registryName?.path, modLoc("block/example_block"))
        singleTexture(
            ItemRegistry.CROWBAR.get().registryName?.path,
            mcLoc("item/generated"),
            "layer0",
            modLoc("item/crowbar")
        )
    }
}