package me.maarten2000ha.transportcraft.setup

import me.maarten2000ha.transportcraft.TransportCraft
import me.maarten2000ha.transportcraft.registry.ItemRegistry
import net.minecraft.world.item.CreativeModeTab
import net.minecraft.world.item.ItemStack

object TransportCraftCreativeTab: CreativeModeTab(TransportCraft.ID){
    override fun makeIcon(): ItemStack {
        return ItemStack(ItemRegistry.CROWBAR.get())
    }
}