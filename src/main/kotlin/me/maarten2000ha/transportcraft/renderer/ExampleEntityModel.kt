package me.maarten2000ha.transportcraft.renderer

import com.mojang.blaze3d.vertex.PoseStack
import com.mojang.blaze3d.vertex.VertexConsumer
import me.maarten2000ha.transportcraft.TransportCraft
import me.maarten2000ha.transportcraft.entities.ExampleEntity
import net.minecraft.client.model.EntityModel
import net.minecraft.client.model.geom.ModelLayerLocation
import net.minecraft.client.model.geom.ModelPart
import net.minecraft.client.model.geom.PartPose
import net.minecraft.client.model.geom.builders.CubeDeformation
import net.minecraft.client.model.geom.builders.CubeListBuilder
import net.minecraft.client.model.geom.builders.LayerDefinition
import net.minecraft.client.model.geom.builders.MeshDefinition
import net.minecraft.resources.ResourceLocation


class ExampleEntityModel<Type : ExampleEntity>(root: ModelPart) : EntityModel<Type>() {
    private val body: ModelPart

    init {
        body = root.getChild("body")
    }

    override fun setupAnim(
        entity: Type, limbSwing: Float, limbSwingAmount: Float, ageInTicks: Float, netHeadYaw: Float, headPitch: Float
    ) {
    }

    override fun renderToBuffer(
        poseStack: PoseStack,
        buffer: VertexConsumer,
        packedLight: Int,
        packedOverlay: Int,
        red: Float,
        green: Float,
        blue: Float,
        alpha: Float
    ) {
        body.render(poseStack, buffer, packedLight, packedOverlay)
    }

    companion object {
        val LAYER_LOCATION = ModelLayerLocation(
            ResourceLocation(TransportCraft.ID, "example_entity"), "main"
        )

        fun createBodyLayer(): LayerDefinition {
            val meshdefinition = MeshDefinition()
            val partdefinition = meshdefinition.root
            val body = partdefinition.addOrReplaceChild(
                "body",
                CubeListBuilder.create().texOffs(0, 0)
                    .addBox(-5.0f, -8.0f, -7.0f, 10.0f, 7.0f, 14.0f, CubeDeformation(0.0f)),
                PartPose.offset(0.0f, 24.0f, 0.0f)
            )
            body.addOrReplaceChild(
                "legs",
                CubeListBuilder.create().texOffs(26, 27)
                    .addBox(5.0f, -3.0f, -6.0f, 2.0f, 3.0f, 2.0f, CubeDeformation(0.0f)).texOffs(17, 27)
                    .addBox(-7.0f, -3.0f, -6.0f, 2.0f, 3.0f, 2.0f, CubeDeformation(0.0f)).texOffs(0, 0)
                    .addBox(-7.0f, -3.0f, 4.0f, 2.0f, 3.0f, 2.0f, CubeDeformation(0.0f)).texOffs(0, 6)
                    .addBox(5.0f, -3.0f, 4.0f, 2.0f, 3.0f, 2.0f, CubeDeformation(0.0f)),
                PartPose.offset(0.0f, 0.0f, 0.0f)
            )
            body.addOrReplaceChild(
                "head",
                CubeListBuilder.create().texOffs(0, 22)
                    .addBox(-2.0f, -10.0f, -11.0f, 4.0f, 3.0f, 4.0f, CubeDeformation(0.0f)).texOffs(17, 22)
                    .addBox(-2.0f, -11.0f, -11.0f, 4.0f, 1.0f, 3.0f, CubeDeformation(0.0f)).texOffs(13, 22)
                    .addBox(-1.0f, -11.0f, -8.0f, 2.0f, 1.0f, 1.0f, CubeDeformation(0.0f)).texOffs(7, 30)
                    .addBox(1.0f, -12.0f, -8.0f, 2.0f, 2.0f, 1.0f, CubeDeformation(0.0f)).texOffs(0, 30)
                    .addBox(-3.0f, -12.0f, -8.0f, 2.0f, 2.0f, 1.0f, CubeDeformation(0.0f)),
                PartPose.offset(0.0f, 0.0f, 0.0f)
            )
            return LayerDefinition.create(meshdefinition, 64, 64)
        }
    }
}