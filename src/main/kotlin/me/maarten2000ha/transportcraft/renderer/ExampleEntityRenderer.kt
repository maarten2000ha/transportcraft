package me.maarten2000ha.transportcraft.renderer

import me.maarten2000ha.transportcraft.TransportCraft
import me.maarten2000ha.transportcraft.entities.ExampleEntity
import net.minecraft.client.renderer.entity.EntityRendererProvider.Context
import net.minecraft.client.renderer.entity.MobRenderer
import net.minecraft.resources.ResourceLocation


class ExampleEntityRenderer<Type : ExampleEntity>(context: Context) : MobRenderer<Type, ExampleEntityModel<Type>>(
    context, ExampleEntityModel(context.bakeLayer(ExampleEntityModel.LAYER_LOCATION)), 0.5f
) {

    val TEXTURE = ResourceLocation(TransportCraft.ID, "textures/entities/example_entity.png")

    override fun getTextureLocation(entity: Type): ResourceLocation {
        return TEXTURE
    }

}
