package me.maarten2000ha.transportcraft.entities

import me.maarten2000ha.transportcraft.registry.EntityRegistry
import net.minecraft.server.level.ServerLevel
import net.minecraft.world.entity.AgeableMob
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.Mob
import net.minecraft.world.entity.ai.attributes.AttributeSupplier
import net.minecraft.world.entity.ai.attributes.Attributes
import net.minecraft.world.entity.ai.goal.*
import net.minecraft.world.entity.animal.Animal
import net.minecraft.world.entity.player.Player
import net.minecraft.world.level.Level

public class ExampleEntity(entity: EntityType<out ExampleEntity>,level: Level): Animal(entity, level) {

    override fun registerGoals() {
        goalSelector.addGoal(0, FloatGoal(this))
        goalSelector.addGoal(1, BreedGoal(this, 1.0))
        goalSelector.addGoal(2, FollowParentGoal(this, 1.2))
        goalSelector.addGoal(3, WaterAvoidingRandomStrollGoal(this, 1.0))
        goalSelector.addGoal(4, LookAtPlayerGoal(this, Player::class.java, 6.0f))
        goalSelector.addGoal(8, RandomLookAroundGoal(this))
    }

    override fun getBreedOffspring(serverLevel: ServerLevel, parent: AgeableMob): AgeableMob? {
        return EntityRegistry.EXAMPLE_ENTITY.get().create(level)
    }

    companion object {
        fun createAttributes(): AttributeSupplier.Builder {
            return Mob.createMobAttributes().add(Attributes.MAX_HEALTH, 50.0)
        }
    }
}
