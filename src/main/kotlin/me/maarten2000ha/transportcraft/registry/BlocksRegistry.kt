package me.maarten2000ha.transportcraft.registry

import me.maarten2000ha.transportcraft.TransportCraft
import me.maarten2000ha.transportcraft.setup.TransportCraftCreativeTab
import net.minecraft.world.item.BlockItem
import net.minecraft.world.item.CreativeModeTab
import net.minecraft.world.item.Item
import net.minecraft.world.level.block.Block
import net.minecraft.world.level.block.state.BlockBehaviour
import net.minecraft.world.level.material.Material
import net.minecraftforge.eventbus.api.IEventBus
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries
import net.minecraftforge.registries.RegistryObject
import java.util.function.Supplier

object BlocksRegistry {
    val BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, TransportCraft.ID)


    val EXAMPLE_BLOCK = registerBlock("example_block") {Block(BlockBehaviour.Properties.of(Material.METAL).strength(9f).requiresCorrectToolForDrops())}


    private fun <T: Block> registerBlock(name: String, block: Supplier<T>): RegistryObject<T>{
        val RegisteredBlock: RegistryObject<T> = BLOCKS.register(name, block)
        registerBlockItem(RegisteredBlock)
        return RegisteredBlock
    }
    private fun <T : Block> registerBlockItem(
        block: RegistryObject<T>
    ): RegistryObject<Item> {
        return ItemRegistry.ITEMS.register(block.id.path) { BlockItem(block.get(), Item.Properties().tab(TransportCraftCreativeTab))}
    }

    fun register(bus: IEventBus) {
        BLOCKS.register(bus)
    }
}