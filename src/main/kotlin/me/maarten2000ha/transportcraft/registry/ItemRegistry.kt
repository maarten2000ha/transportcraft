package me.maarten2000ha.transportcraft.registry

import me.maarten2000ha.transportcraft.TransportCraft
import me.maarten2000ha.transportcraft.items.CrowbarItem
import net.minecraft.world.item.Item
import net.minecraftforge.eventbus.api.IEventBus
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries
import net.minecraftforge.registries.RegistryObject

object ItemRegistry {
    val ITEMS: DeferredRegister<Item> = DeferredRegister.create(ForgeRegistries.ITEMS, TransportCraft.ID)

    fun register(bus: IEventBus) {
        ITEMS.register(bus)
    }

    val CROWBAR: RegistryObject<Item> = ITEMS.register<Item>("crowbar") { CrowbarItem }
}