package me.maarten2000ha.transportcraft.registry

import me.maarten2000ha.transportcraft.TransportCraft
import me.maarten2000ha.transportcraft.entities.ExampleEntity
import me.maarten2000ha.transportcraft.entities.train.TrainEntity
import net.minecraft.resources.ResourceLocation
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.MobCategory
import net.minecraftforge.eventbus.api.IEventBus
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries

object EntityRegistry {
    val ENTITIES: DeferredRegister<EntityType<*>> = DeferredRegister.create(ForgeRegistries.ENTITIES, TransportCraft.ID)


    fun register(bus: IEventBus) {
        ENTITIES.register(bus)
    }

//    val TRAIN_ENTITY = ENTITIES.register("train_entity") {
//        EntityType.Builder.of(::TrainEntity, MobCategory.MISC)
//            .build(ResourceLocation(TransportCraft.ID, "train_entity").toString())
//    }

    val EXAMPLE_ENTITY = ENTITIES.register("example_entity") {
        EntityType.Builder.of(::ExampleEntity, MobCategory.CREATURE)
            .build(ResourceLocation(TransportCraft.ID, "example_entity").toString())
    }
}